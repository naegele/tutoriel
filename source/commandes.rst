====================
Bibliothèque Brickpi
====================

Nous allons durant ce chapitre voir les différentes ressources mises en place pour faire fonctionner nos moteurs.
A titre d'introduction, nous regarderons le fichier LEGO-Motor_Test.py dans le dossier BrickPi_Python/Sensor_Example, que vous pouvez tout d'abord exécuter.

Vous observerez alors les moteurs tourner en avant puis en arrière, indéfiniment.

Regardons le code 4.1

..  code-block:: python
    :caption: LEGO-Motor_Test.py
    :linenos:
    
    from BrickPi import *
    
    BrickPiSetup() 
    
    BrickPi.MotorEnable[PORT_A] = 1 #active le moteur du port A
    BrickPi.MotorEnable[PORT_B] = 1 #active le moteur du port B
    BrickPi.MotorEnable[PORT_C] = 1
    BrickPi.MotorEnable[PORT_D] = 1

    BrickPiSetupSensors() #Envoie les propriétés des moteurs à BrickPi

    power = 0

    while True:
        print "Running Forward"
        power = 200
        BrickPi.MotorSpeed[PORT_A] = power #Met la vitesse de ce moteur à 200
        BrickPi.MotorSpeed[PORT_B] = power  
        BrickPi.MotorSpeed[PORT_C] = power 
        BrickPi.MotorSpeed[PORT_D] = power 

        ot = time.time()
        while(time.time() - ot < 3):    
        BrickPiUpdateValues()    # Met à jour l'état du moteur
	    time.sleep(.1)

        print "Running Backward"
        power = -200
        BrickPi.MotorSpeed[PORT_A] = power
        BrickPi.MotorSpeed[PORT_B] = power
        BrickPi.MotorSpeed[PORT_C] = power
        BrickPi.MotorSpeed[PORT_D] = power
    
        ot = time.time()
        while(time.time() - ot < 3):   
            BrickPiUpdateValues()  
            time.sleep(.1)

..  hint::
    Les vitesses maximums et minimums sont respectivement 255 et -255. Vous pouvez indiquer des vitesses supérieurs, cela ne changera cependant pas la vitesse réelle.

Ce code fait que les moteurs vont en avant durant 3 secondes, puis fait une pause 0.1 sec (qui permet au Brickpi de changer la valeur de sa vitesse), puis va en arrière pendant 3 secondes et ainsi de suites.

Il est très intéressant de prendre cet exemple car il permet de connaître la structure général de notre futur programme. 
Dans les lignes 5-8 et 17-20, il y en a évidemment certaines que vous pouvez enlever selon les ports sur lesquels sont branchés les moteurs selon le schéma du chapitre 2.
Une des choses primordiales à retenir est le ``BrickPiUpdateValues()`` de la ligne 24 et 36, c'est grâce à lui que nous notre nouvelle direction/puissance est prise en compte.

Exercice
---------
    
Essayez de créer un fichier qui nous permettra de changer la direction selon la valeur d'un paramètre.
Vous devrez pour cela faire attention aux ports sur lesquels vous avez branché les moteurs et aux éléments que nous avons vu auparavant.

Correction 4.2
***************

Voici une correction possible

..  code-block:: python
    :caption: motor.py
    :linenos:
    
    from BrickPi import *
    
    BrickPiSetup()
    BrickPi.MotorEnable[PORT_B] = 1 
    BrickPi.MotorEnable[PORT_C] = 1
    BrickPiSetupSensors()
    
    
    def direction(d):
	    while d==1:
	        BrickPi.MotorSpeed[PORT_B] = 200
            BrickPi.MotorSpeed[PORT_C] = 200
            BrickPiUpdateValues()
    
    
	    while d==2:
		    BrickPi.MotorSpeed[PORT_B] = -200
            BrickPi.MotorSpeed[PORT_C] = -200
            BrickPiUpdateValues()
    
    
	    while d==3:
		    BrickPi.MotorSpeed[PORT_B] = -200
            BrickPi.MotorSpeed[PORT_C] = -200
            BrickPiUpdateValues()
    
    
	    while d==4:
		    BrickPi.MotorSpeed[PORT_B] = -200
            BrickPi.MotorSpeed[PORT_C] = -200
            BrickPiUpdateValues()

Il n'y a je pense pas grand chose à expliquer.
Nous garderons ce code lors du TP suivant.


TP Serveur et commandes
-----------------------

Nous allons au cours de ce TP, tenter de créer un serveur qui puisse déjà faire rouler notre Rover.
Nous aurons besoin d'au minimum trois fichier, le fichier HTML contenant des boutons, du serveur et finalement du fichier que nous avons créé dans l'exercice précédent.
Il n'est pas nécessaire de le faire pour toutes les directions, essayez déjà pour une.

..  hint::
    
    Les boutons de la page HTML seront reliés à une redirection mais enverront l'information au serveur.


-- TP EFFECTUE --


Vous avez pu rencontrer un problème durant ce TP, cela est bien voulu et permet d'introduire une nouvelle partie de ce chapitre: La programmation asyncrone.
