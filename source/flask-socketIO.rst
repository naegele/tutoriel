###############
flask-socketIO
###############

Mise en place
=============

Utiliser flask-socketIO permet à notre application de communiquer en temps réel par différentes techniques. Nous allons voir la plus connue d'entre elle: les WebSocket
Jusqu'à maintenant nous avons utilisé la communication asynchrone (figure 4.1). Cependant ce système n'est pas immédiat, si nous voulons, dans notre cas faire tourner des moteurs, cela demande de rafraichir la page à chaque requête.
La particularité de WebSocket est d'avoir une communication toujours ouverte entre le serveur et le client.

figure 5.1

.. image:: figures/serverclient2.png
    :align: center
.. image d'ordinateur tiré de http://lecrabeinfo.net/wp-content/uploads/2013/03/connaitre-composants-ordinateur-configuration-pc-dcf0bb.png )


..  Important:: 
        WebSocket ne supporte pas tous les navigateurs, vérifiez que vous utilisez bien ces versions de navigateur ou des versions ultérieures: 
        Internet Explorer 5.5+,
        Safari 3+,
        Google Chrome 4+,
        Firefox 3+
        ou Opera 10.61+.

Vous pouvez simplement l'installer grâce à pip par la commande:


..  code-block:: python
    
    pip install flask-socketio

Regardons tout d'abord comment l'implémenter dans le serveur.


..  code-block:: python
    :caption: server.py
    :linenos:
    
    from flask import *
    from flask_socketio import SocketIO

    app = Flask(__name__)
    
    #Initialisation de SocketIO
    socketio = SocketIO(app)

    if __name__ == '__main__':
    #démarrage du server web
        socketio.run(app)

Notons que ``socketio.run()`` remplace le ``app.run()`` du Flask standard.

Il faut désormais s'occuper du client. Pour cela nous avons deux possibilités:

Soit vous l'installer en local et n'êtes donc pas dépendant de la connection internet, soit vous le utilisez le CDN qui permet d'utiliser le code de socket.io stocké sur un serveur.

En local
--------

Vous l'installez avec: `` sudo apt-get install https://cdn.socket.io/socket.io-1.3.7.js ``. Puis vous pouvez créer un dossier js (si cela n'a pas été fait pour JQuery) et ajoutez dans server.py

..  code-block:: python
    :caption: server.py
    
    @app.route("/js/<path:path>")
    def send_js(path):
    return send_from_directory('js', path)

Cela permettra au serveur de voir où les fichiers se trouvent.
Chez le client, cela se présente ainsi:

..  code-block:: html
    :caption: index.html
    :linenos:
    
    <script src="js/socket.io-1.3.7.js"></script>


Par le CDN
----------

Vous avez simplement à vous occupez du client, car ce fichier n'a pas besoin d'être indiqué du côté du serveur.

..  code-block:: html
    :caption: index.html
    :linenos:
    
    <script src="https://cdn.socket.io/socket.io-1.3.7.js"></script>



Dans la suite du cours j'utiliserai la version local.

    
Utilisation
===========

Structure principale
--------------------

Veuillez copier les codes 2.5 et 2.6 à titre d'introduction.



..  code-block:: python
    :caption: server.py
    :linenos:
    
    from flask import *
    from flask_socketio import SocketIO


    app = Flask(__name__)
    socketio = SocketIO(app)
    
    @app.route("/js/<path:path>")
    def send_js(path):
    return send_from_directory('js', path)
    
    @app.route("/") 
    def index():
    return render_template("index.html")
    
    @socketio.on("connection")
    def connection():
	print("Une personne est connectée")


    if __name__ == "__main__":
	socketio.run(app, host="0.0.0.0", port=8080, debug=False)
	
	
..  code-block:: html
    :caption: index.html
    :linenos:
    
    <script src="js/jquery-1.10.2.js"></script>
    <script src="https://cdn.socket.io/socket.io-1.3.7.js"></script>
    
    <script>
	var socket = io.connect("http://localhost:8080");    
    socket.on("connect", function() {
        socket.emit("connection"});
    });
	
    </script>

Veuillez désormais exécuter le server.py et regarder sur le localhost
    
Vous devriez voir apparaître sur le terminal le message "Une personne est connectée"
Du côté de la page HTML, à la ligne 5, nous avons tout d'abord connecter la socket à la page.
Puis, lorsque l'évenement de connection d'un client ``"connect"`` est enclenché, cela envoie l'information ``"connection"`` au serveur. Le serveur l'interprète dans server.py, à la ligne 16 et renvoie alors à la console ``"Une personne est connectée"``.

.. hint::
    Vous pouvez remplacez ``var socket = io.connect("http://localhost:8080");`` par  ``var socket = io.connect("http://" + location.hostname + ":8080");``. Cela permet de ne pas avoir à le modifier si jamais vous changer d'hostname.
    (Par exemple si vous utilisez votre ordinateur pour ouvrir la page au lieu de votre RPi.)
    
Grâce à cela nous pouvons désormais nous remettre sur notre projet!

