###########################
Application web avec Flask
###########################

Fonctionnement d'une application web
====================================
Nous allons commencer par présente par présenter quelques notions théoriques sur les interactions entre serveur et client.
Lorsque vous vous rendez sur un site internet vous êtes un client, il y a derrière celui-ci un ou plusieurs ordinateurs nommé serveurs qui vous permettent de voir la page que vous avez recherché.
Cet échange se fait par un protocole HTTP: lorsque le client demande une page, le serveur la lui envoie. On la 


La requête:
-----------

figure 3.1

.. image:: figures/serverclient.png
    :align: center
.. image d'ordinateur tiré de http://lecrabeinfo.net/wp-content/uploads/2013/03/connaitre-composants-ordinateur-configuration-pc-dcf0bb.png )

Dans la figure 4.1, le client fait une requête de type ``GET``, cela signifie qu'il demande une page, et indique le chemin de celle-ci, là index.html. 
Il y a un autre type de requête: la requête ``POST`` si le client veut envoyer des données comme les réponses d'un questionnaire au serveur. 
La réponse:
Le serveur envoie évidement dans notre exemple la page index.html, il pourrait très bien envoyer des documents, images ou autres. Mais il envoie également un code d'erreur (ici 200),
nous allons justement en voir quelques uns.


.. list-table:: 
    :widths:15 10 30
    :header-rows: 1
    
    *erreur 200


.. list-table:: Codes d'erreurs HTTP
   :widths: 15 30
   :header-rows: 1

   * - Nom de l'erreur
     - Description
   * - erreur 200
     - Tout a bien fonctionner
   * - erreur 404
     - Le chemin demandé est introuvable
   * - erreur 500
     - Il y a une erreur interne au serveur
   * - erreur 401
     - l'accès à la page n'est pas autorisé
   * - erreur 400
     - La requête est malformée et le serveur n'a donc pas pu la traiter
     

Microframework flask
=====================

Installation
------------
Flask est un microframework basé sur Werkzeug et Jinja 2 pour python. Vous pouvez le télécharger avec la commande

..  code-block:: python

    $ pip install Flask

Veuillez vous référer à cette rubrique.

Il faut tout d'abord importer le microframework

..  code-block:: python
    :caption: server.py
    :linenos:
    
    from flask import *

Premiers pas
------------
    
Regardons le code suivant:

..  code-block:: python
    :caption: server.py
    :linenos:
    
    from flask import *
    app = Flask(__name__)
    
    @app.route("/")
    def hello():
        return "<h1>Hello world !</h1>"
    
    
    if __name__ == "__main__":
        app.run(debug=True)
        
Si vous exécutez ce code un serveur démarrera sur le réseau local

..   code-block:: python

    $ python server.py
        * Running on http://localhost:5000/

Lorsque vous vous connectez grâce au navigateur web de votre RPi, vous verrez simplement "hello World".
Du côté du terminal vous devriez avoir une indication analogue à

..  code-block:: python

    127.0.0.1 - - [28/Dec/2015 15:12:03] "GET / HTTP/1.1" 200-
    
On retrouve là certaines informations que je vous avais donné.
Pour arrêter le serveur, faites "CTRL + C"

Tout d'abord observons la structure principale de l'application:
A la ligne 2, on instancie l'objet app qui est notre application, notre site web. On le passe en paramètre ``__name__`` (Cela restera ainsi tout au long du tutoriel), il vaut dans notre cas "__main__".
La ligne 10 permet d'enclencher notre app si son ``__name__`` est ``"__main__"`` qui est le cas.
Remarquons le debug=True, il permet d'activer le mode debug et d'ainsi pouvoir plus efficacement trouver les problèmes dans le codes que vous rencontrerez durant les exercices.

Au coeur de l'application nous trouvons le décorateur ``@app.route("/")`` qui permet d'indiquer le chemin pour la fonction ``hello()``. Cette dernière retourne Hello world !

Cependant lorsque vous aurez des pages plus complexe, ce ne sera pas très pratique d'avoir le code HTML dans le code flask.
Ainsi, je vous propose de créer un fichier hello.html dans un dossier templates à côté de votre serveur. Ce dossier permettra de stocker séparément le côté client et le côté serveur.
Dans ce fichier nous pouvons écrire tout simplement:

..  code-block:: html
    :caption: hello.html
    :linenos:
    
    <h1>Hello world !</h1>

Maintenant du côté du serveur nous ne souhaitons plus renvoyé le code HTML mais la page, donc nous remplacerons notre précédente fonction par ceci

..  code-block:: python
    :caption: server.py
    :linenos:
    
    @app.route("/")
    def hello():
        return render_template("index.html")

Si vous lancez le serveur et allez voir la page, vous observerez le même résultat qu'auparavant.

Personnaliser les pages d'erreur
--------------------------------
Si vous avez déjà rentrer une URL introuvable, vous avez pu vous rendre compte qu'une page assez austère était envoyée.
Nous pouvons envoyer une page qui permettra que le client reçoive une page avec le même style graphique et qu'il puisse se rediriger sur les autres pages facilement par le code suivant:

..  code-block:: python
    :caption: server.py
    :linenos:
    
    @app.errorhandler(404)
    def not_found(error):
        return "Une page d'erreur", 404

Flask est assez simple pour cela, nous avons juste à utiliser le décorateur ``@app.errorhandler()``. Sa différence avec app.route est qu'il l'associe à une erreur et non à une URL.
Dans la ligne 3, vous remarquerez que nous avons mis 404, bien que ce ne soit pas nécessaire, ça permet que le terminal renvoie bel et bien une erreur 404 et non une erreur 200.

Redirection
-----------

On pourrait à la place d'envoyer une page au client qui a écrit un URL introuvable, lui renvoyer sur notre page index.html grâce à la fonction redirect.

..  code-block:: python
    :caption: server.py
    :linenos:
    
    @app.errorhandler(404)
    def not_found(error):
        return redirect("/")

Sauf que cela n'est pas très pratique; car si on veut modifier la route de l'index.html on devra le faire à plusieurs reprises, et donc éventuellement oublier certains endroits.

La méthode encouragée est de ne pas indiquer la route, mais le nom de la vue que nous avions défini, il est en effet beaucoup plus rare de la changer.
Si vous êtes resté dans le même fichier depuis le début, nous l'avons donc défini comme hello

..  code-block:: python
    :caption: server.py
    :linenos:
    
    @app.errorhandler(404)
    def not_found(error):
        return redirect(url_for("hello"))
    


Les méthodes HTTP
-----------------
Nous avons vu auparavant les requêtes ``POST`` et ``GET`` de manière théorique, il est désormais temps de les utiliser dans le code.
Reprenons notre server.py précédent et ajoutons lui une nouvelle route:

..  code-block:: python
    :caption: server.py
    :linenos:
    
    @app.route('/example', methods=['GET', 'POST'])
    def example():
    if request.method == 'GET':
    #faire une certaine action
    else
    #faire une certaine action

Par là, on entends que si le client demande une page, le serveur fera une certaine action. POST servirait lorsque le client envoie, par exemple un formulaire.


.. voyons si on veut pas mettre un exercice là